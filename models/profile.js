const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    company: {
        type: String
    },
    website: {
        type: String
    },
    minibio: {
        type: String
    },
    location: {
      type: String
    },
    social: {
        youtube: {
            type: String
        },
        twitter: {
            type: String
        },
        facebook: {
            type: String
        },
        linkedin: {
            type: String
        },
        instagram: {
            type: String
        },
        github: {
            type: String
        },
    },
        detailCompany: [
            {
              bulk: {
                type: String,
                required: true
              },
              metier: {
                type: String,
                required: true
              },
              productOrService: {
                type: String,
                required: true
              }
            }
        ],
    
    date: {
      type: Date,
      default: Date.now
    }
}, { autoCreate : true })


module.exports = mongoose.model('profile', ProfileSchema);

