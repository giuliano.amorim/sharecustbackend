const express = require('express')
const User = require('../../models/user')
const Profile = require('../../models/profile')
const bcrypt = require('bcryptjs')
const {check, validationResult } = require('express-validator')
const auth = require('../../middleaware/auth')
const router = express.Router()



// @route    GET /user
// @desc     ALL user
// @access   Private
router.get('/', auth, async(req, res, next) => {
    try {
    const user = await User.find({})
    res.json(user)
    }
        catch (err) {
            res.status(500).send({"err" : "Server Error"})
        }
    }
)


// @route    GET /user/:userId   
// @desc     DETAIL user
// @access   Public
router.get('/:userId', [], async(req, res, next) => {
    try {
        const id = req.params.userId
        const user = await User.findOne({_id : id})
            if(user) {
                res.json(user)
            }
            else {
                res.status(404).send({"error" : "user not found"})
            }
        } 
        catch(err) {
            console.error(err.message)
            res.status(500).send({"err" : "Server error"})
        }
})

// @route    POST /user
// @desc     CREATE user
// @access   Public
router.post('/', [
    
    check('name').not().isEmpty(),
    check('cnpj').not().isEmpty(),
    check('email', 'email is not valid').isEmail(),
    check('colaborar').not().isEmpty(),
    check('exigir').not().isEmpty(),
    check('password', 'Please enter a password with 6 or more characters').isLength({min : 6})

],     
    async(req, res, next) => {  
    try {
        
        let {name, cnpj, email, colaborar, exigir, password, is_active, is_admin} = req.body
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({errors : errors.array()})
        }
        else {
            let user = new User ({ name, cnpj, email, colaborar, exigir, password, is_active, is_admin })
            const salt = await bcrypt.genSalt(10)
            user.password = await bcrypt.hash(password, salt)
            await user.save()
            if (user.id) {
                res.status(201).json(user)
            }
        }
    }
    catch(err) {
        console.error(err.message)
        res.status(500).send({"err" : "Server Error"})
    }  
})


// @route    DELETE /user/:userId
// @desc     DELETE user
// @access   Private
router.delete('/:userId',[], async(req, res, next) => {
    try {
        const id = req.params.userId
        const user = await User.findOneAndDelete({_id : id})
        await Profile.findOneAndDelete({user : id})
        if (user) {
            res.json(user)
        }
        else {
            res.status(404).send({"error" : "user doesn't found"})
        }
    }
    catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server error"})
    }
})



// @route    PUT /user/:userId
// @desc     EDIT user
// @access   Public
router.put('/:userId', [

    check('email', 'email invalid').isEmail(),
    check('name').not().isEmpty(),
    check('password', 'Please enter a password with 6 or more characters').isLength({min : 6}),
    check('cnpj').not().isEmpty(),
    check('colaborar').not().isEmpty(),
    check('exigir').not().isEmpty() 

], 

    async(req, res, next) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({errors : errors.array() })
            }
            const id = req.params.userId
            let {name, cnpj, email, colaborar, exigir, password, is_active, is_admin} = req.body
            let update = {name, cnpj, email, colaborar, exigir, password, is_active, is_admin}

            const salt = await bcrypt.genSalt(10)
            update.password = await bcrypt.hash(password, salt)

            let user = await User.findOneAndUpdate({_id : id}, update, { new: true })
            if (user) {
                res.json(user)
            }
            else {
                res.status(404).send ({"error" : "user not found"})
            }
        }
            catch(err) {
                console.error(err.message)
                res.status(500).send({"error" : "Server Error"})
            }

        })
            


// @route    PATCH /user/:userId
// @desc     PARTIAL EDIT user
// @access   Public
router.patch('/:userId', [], async(req, res, next) => {
        try {
           const errors = validationResult(req)
           if (!errors.isEmpty()) {
               res.status(400).send({errors: errors.array()})
               return
           }
            const id = req.params.userId
            const salt = await bcrypt.genSalt(10)

            let bodyReq = req.body

            if(bodyReq.password) {
                bodyReq.password = await bcrypt.hash(bodyReq.password, salt)
            }
            console.log(bodyReq)
            const update = {$set : bodyReq}

            const user = await User.findByIdAndUpdate(id, update, {new : true})
            if (user) {
                res.send(user)
            } 
                else {
                res.status(404).send({"error" : "user doesn't exist"})
                }
            }
        catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "server error"})
        }
})


module.exports = router