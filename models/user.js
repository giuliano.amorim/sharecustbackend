const bcrtpt = require('bcryptjs')
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true
    },

    cnpj : {
        type : Number,
        required : true
    },

    email : {
        type : String,
        required : true,
        unique : true
    },

    password : {
        type : String,
        required : true,
        select: false
    },

    colaborar : {
        type : String,
        required : true
    },

    exigir : {
        type : String,
        required : true
    },

    is_active: {
        type: Boolean,
        default: true
        },
        
    is_admin: {
        type: Boolean,
        default: false
    },

    date: {
        type: Date,
        default: Date.now
    }
})


module.exports = mongoose.model('user', UserSchema)
