## Versão

1.0.0

## Pré Requisitos
* node

## Instalação

''' npm install '''

## Root API:

/api/

##  Link para o repositório do projeto no gitLab:

 https://gitlab.com/giuliano.amorim/sharecust

 ## Link para o projeto no Trello:

 https://trello.com/b/0UAr1fgp/projetoindividual01-sharecust


 ## Tecnologias utilizadas:

 NodeJs
 React
 MongoDB
 Express
 Bcryptjs
 Cors
 Jwt
 Bootstrap
 Reactstrap
 Styled-components
 Font Awesome


  
## Deploy:

https://sharecust.herokuapp.com/

## GitLab:
BackEnd: https://gitlab.com/giuliano.amorim/sharecustbackend.git
FrontEnd: https://gitlab.com/giuliano.amorim/sharecustfrontend.git

## Prints das telas:

/home/giuliano/Documentos/Infnet Bootcamp 2020/Projetos_Individuais/projeto1/patrocinadores/back/prints

## Trabalhos futuros:

Tornar mais responsivo.
Melhorar a estilização.

