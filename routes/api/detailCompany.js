const express = require('express');
const Profile = require('../../models/profile');
const User = require('../../models/user');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const auth = require('../../middleaware/auth')


// @route    POST /detailCompany
// @desc     CREATE detailCompany
// @access   Private
router.post('/', auth, async (req, res, next) => {
    try {

    const id = req.user.id
    const profile = await Profile.findOneAndUpdate({user : id}, { $push : { detailCompany : req.body }} , { new: true })
    if (profile) {
      res.json(profile)
    } else {
      res.status(404).send({ "error": "user not found" })
    }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": "Server Error" })
    }
  })



// @route    DELETE /detailCompany
// @desc     DELETE detailCompany
// @access   Private
router.delete('/', auth, async (req, res, next) => {
    try {
      const id = req.user.id
      const profile = await Profile.findOneAndUpdate({ user: id }, { $pull: { detailCompany: req.body } }, { new: true })
      if (profile) {
        res.json(profile)
      } else {
        res.status(404).send({ "error": "user not found" })
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": "Server Error" })
    }
  })
  

module.exports = router;
