const cool = require('cool-ascii-faces')
const express = require('express')
const bodyParser = require ('body-parser')
const connectDB = require('./config/db')
const path = require('path')

const app = express()
const PORT = process.env.PORT || 3005
var cors = require('cors')


// Middleware
app.use(express.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.use(cors())


// Connect Database
connectDB()


app.use (function (req, res, next) {
    var schema = (req.headers['x-forwarded-proto'] || '').toLowerCase();
    if (req.headers.host.indexOf('localhost') < 0 && schema !== 'https') {
        // request was via http, so redirect to https
        res.redirect('https://' + req.headers.host + req.url);
    }
    next();
});


// Routes
app.use('/user', require('./routes/api/user'))
app.use('/auth', require('./routes/api/auth'))
app.use('/profile', require('./routes/api/profile'))
app.use('/detailCompany', require('./routes/api/detailCompany')) 

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname))
})
app.get('/cool', (req, res) => res.send(cool()))  

app.listen(PORT, () => {console.log(`Server Connected at ${PORT}.. wait...`)})
